## Fusepump Connector
This module loads the fu fusepump library into your site. The Javascript library will crawl the DOM and find any widget containers to populate with Lightboxes.

## Additional steps to fully implement

####Installation and use

Install the module as usual.

After the installation you'll have a new field type called "field_fusepump". You can add it directly to your 
Content Type or to another fieldable entity you're using in your site.

The next time you create a new element of this content type or that contains the fieldable entity, you'll see the new field
to add the fusepump HTML Tag. It's only a text filed.

Into this field you should add the HTML Tag received. It should be something like:

```
<div class="field-item"><div id="fusepump-0-61" class="fusepump-widget"></div>
<button class="button primary extended fusepump-link buy-now-btn" data-id="v3w3o16" data-fallback="" target-id="fusepump-0-61">KOOP NU</button>
</div>
```

...where data-id is the provided lightbox or widget ID.

In order to overwrite configured EANs in the CTA these have to be passed in as per the below example:

```
<div class="field-item"><div id="fusepump-0-61" class="fusepump-widget"></div>
<button class="button primary extended fusepump-link buy-now-btn" data-id="v3w3o16" data-fallback="" target-id="fusepump-0-61">KOOP NU</button>
</div>
```

##### Override template

The module comes with a simple twig template called "fusepump-widget.html.twig"
you can copy this template in your theme and customize it.
The more important thing to take in account is the way in which the HTML Tag is rendered into this template

{{ html_tag | raw }}

Don't forget the use of the "raw" filter to render properly the tag, otherwise the HTML Tasg as plain text will be output.
