<?php

namespace Drupal\fusepump_connector\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'field_channelsight' formatter.
 *
 * @FieldFormatter(
 *   id = "fusepump_formatter",
 *   module = "fusepump_connector",
 *   label = @Translation("FusePump formatter"),
 *   field_types = {
 *     "field_fusepump"
 *   }
 * )
 */
class FusePumpFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    foreach ($items as $delta => $item) {
      $element[$delta] = [
        '#theme' => 'fusepump_widget',
        '#id' => $item->id,
        '#fallback' => $item->fallback,
        '#field_id' => $item->getEntity()->id(),
        '#delta' => $delta,
        '#attached' => [
          'library' => 'fusepump_connector/fusepump',
        ],
      ];
    }

    return $element;
  }

}
